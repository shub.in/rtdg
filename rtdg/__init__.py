from rtdg.config import read_config
from rtdg.app import make_app


if __name__ == '__main__':
    app = make_app(read_config())
