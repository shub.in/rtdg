from fastapi import APIRouter, Depends, Request, WebSocket, WebSocketDisconnect
from fastapi.templating import Jinja2Templates

from rtdg.config import Config
from rtdg.dependencies import config_dep, templates_dep, tickers_store_dep, tickers_store_ws_dep, \
    ws_conn_manager_dep
from rtdg.store import TickersStore
from rtdg.ws import ConnectionManager

router = APIRouter()


@router.get('/')
async def home(request: Request,
               templates: Jinja2Templates = Depends(templates_dep),
               config: Config = Depends(config_dep),
               tickers_store: TickersStore = Depends(tickers_store_dep)):
    return templates.TemplateResponse('index.html', {
        'request': request,
        'tickers_keys':  tickers_store.tickers_keys,
        'buffer_size': config.tickers_buffer_size
    })


@router.websocket('/ws/{ticker_key}')
async def websocket_endpoint(ticker_key: str,
                             connection: WebSocket,
                             tickers_store: TickersStore = Depends(tickers_store_ws_dep),
                             manager: ConnectionManager = Depends(ws_conn_manager_dep)):
    if ticker_key not in tickers_store.tickers_keys:
        await connection.accept()
        await connection.close()
        return
    await manager.connect(ticker_key, connection)
    await connection.send_json(tickers_store.get_buffer_for(ticker_key))
    try:
        while True:
            await connection.receive_bytes()
    except WebSocketDisconnect:
        manager.disconnect(ticker_key, connection)
