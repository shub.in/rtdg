import asyncio
from time import time

from rtdg.app import App


async def ticker_worker(app: App):
    while True:
        await asyncio.sleep(1)
        start = time()
        app.tickers_store.tick()
        duration = time() - start
        result = f'{duration}'
        if 1 - duration > 0:
            result += f' | SLEEP: {1-duration}'
            await asyncio.sleep(1 - duration)
        for ticker_key in app.ws_manager.tickers_keys:
            await app.ws_manager.broadcast(ticker_key, app.tickers_store.get_value_for(ticker_key))
