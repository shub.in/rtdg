from fastapi import Request, WebSocket
from fastapi.templating import Jinja2Templates

from rtdg.config import Config
from rtdg.store import TickersStore


async def templates_dep(request: Request) -> Jinja2Templates:
    return request.app.templates


async def config_dep(request: Request) -> Config:
    return request.app.config


async def tickers_store_dep(request: Request) -> TickersStore:
    return request.app.tickers_store


async def tickers_store_ws_dep(connection: WebSocket) -> TickersStore:
    return connection.app.tickers_store


async def ws_conn_manager_dep(connection: WebSocket) -> TickersStore:
    return connection.app.ws_manager
