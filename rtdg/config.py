from enum import Enum

from pydantic import BaseSettings, Field


class TickerStoreType(str, Enum):
    DEFAULT = 'default'
    NUMPY = 'numpy'
    DEQUE = 'deque'


class Config(BaseSettings):
    host: str = Field(default='127.0.0.1')
    port: int = Field(default='8080')
    path_prefix: str = Field(default='')
    tickers_amount: int = Field(default=100, gt=0)
    tickers_buffer_size: int = Field(default=100, gt=0)
    tickers_store: TickerStoreType = Field(default=TickerStoreType.DEQUE)
    static_path: str = Field(default='static')
    templates_path: str = Field(default='templates')
    https: int = Field(default=0)

    class Config:
        env_prefix = 'RTDG_'
        env_file = '.env'
        env_file_encoding = 'utf-8'


def read_config() -> Config:
    return Config()
