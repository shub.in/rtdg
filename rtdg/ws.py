from typing import Dict, List, Iterable

from fastapi import WebSocket


class ConnectionManager:
    _TICKER_CONNECTIONS: Dict[str, List[WebSocket]]

    def __init__(self):
        self._TICKER_CONNECTIONS = {}

    @property
    def tickers_keys(self) -> Iterable[str]:
        return self._TICKER_CONNECTIONS.keys()

    async def connect(self, ticker_key: str, connection: WebSocket):
        await connection.accept()
        if ticker_key not in self._TICKER_CONNECTIONS:
            self._TICKER_CONNECTIONS[ticker_key] = [connection]
        else:
            self._TICKER_CONNECTIONS[ticker_key].append(connection)

    def disconnect(self, ticker_key: str, connection: WebSocket):
        if ticker_key not in self._TICKER_CONNECTIONS:
            return
        if connection not in self._TICKER_CONNECTIONS[ticker_key]:
            return
        self._TICKER_CONNECTIONS[ticker_key].remove(connection)
        if len(self._TICKER_CONNECTIONS[ticker_key]) == 0:
            del self._TICKER_CONNECTIONS[ticker_key]

    async def broadcast(self, ticker_key: str, message: int):
        if ticker_key not in self._TICKER_CONNECTIONS:
            return
        for connection in self._TICKER_CONNECTIONS[ticker_key]:
            await connection.send_json(message)
