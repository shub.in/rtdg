from typing import Dict, Optional, List

import numpy as np
from numpy.typing import NDArray

from .default import TickersStore


class NumpyTickersStore(TickersStore):
    _TICKERS_BUFFER: Dict[str, NDArray[int]]
    _SHIFT_INDEX = 0

    def _init_tickers(self):
        for index in range(self._TICKERS_AMOUNT):
            tkey = self._key_from_index(index)
            self._TICKERS[tkey] = 0
            self._TICKERS_BUFFER[tkey] = np.zeros(self._TICKERS_BUFFER_SIZE, dtype=np.int16)

    def tick(self):
        for key in self.tickers_keys:
            self._TICKERS[key] += self._generate_movement()
            self._TICKERS_BUFFER[key][self._SHIFT_INDEX] = self._TICKERS[key]
        self._SHIFT_INDEX += 1
        if self._SHIFT_INDEX >= self._TICKERS_BUFFER_SIZE:
            self._SHIFT_INDEX = 0

    def get_buffer_for(self, ticker_key: str) -> Optional[List[int]]:
        if ticker_key not in self._TICKERS_BUFFER:
            return None
        left = self._TICKERS_BUFFER[ticker_key][self._SHIFT_INDEX:]
        right = self._TICKERS_BUFFER[ticker_key][:self._SHIFT_INDEX]
        return np.concatenate((left, right)).tolist()
