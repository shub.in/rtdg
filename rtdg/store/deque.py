from typing import Dict, Optional, List
from collections import deque

from .default import TickersStore


class DequeTickersStore(TickersStore):
    _TICKERS_BUFFER: Dict[str, deque[int]]

    def _init_tickers(self):
        for index in range(self._TICKERS_AMOUNT):
            tkey = self._key_from_index(index)
            self._TICKERS[tkey] = 0
            self._TICKERS_BUFFER[tkey] = deque([0], maxlen=self._TICKERS_BUFFER_SIZE)

    def tick(self):
        for key in self.tickers_keys:
            self._TICKERS[key] += self._generate_movement()
            self._TICKERS_BUFFER[key].append(self._TICKERS[key])

    def get_buffer_for(self, ticker_key: str) -> Optional[List[int]]:
        if ticker_key not in self._TICKERS_BUFFER:
            return None
        return list(self._TICKERS_BUFFER.get(ticker_key))
