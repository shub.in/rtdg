from typing import Dict, List, Optional, Iterable
from random import random


class TickersStore:
    _TICKERS: Dict[str, int]
    _TICKERS_BUFFER: Dict[str, List[int]]

    def __init__(self, amount: int = 100, buffer_size: int = 10):
        if amount <= 0:
            raise Exception('amount should be greater than 0')
        if buffer_size <= 0:
            raise Exception('buffer_size should be greater than 0')
        self._TICKERS_AMOUNT = amount
        self._TICKERS_BUFFER_SIZE = buffer_size
        self._TICKERS = {}
        self._TICKERS_BUFFER = {}
        self._init_tickers()

    @staticmethod
    def _generate_movement() -> int:
        return -1 if random() < 0.5 else 1

    def _pad_zeroes(self) -> int:
        return len(str(self._TICKERS_AMOUNT - 1))

    def _key_from_index(self, index: int) -> str:
        ticker_index = str(index).zfill(self._pad_zeroes())
        ticker_key = f'ticker_{ticker_index}'
        return ticker_key

    def _init_tickers(self):
        for index in range(self._TICKERS_AMOUNT):
            tkey = self._key_from_index(index)
            self._TICKERS[tkey] = 0
            self._TICKERS_BUFFER[tkey] = [self._TICKERS[tkey]]

    @property
    def tickers_keys(self) -> Iterable[str]:
        return self._TICKERS.keys()

    def tick(self):
        for key in self.tickers_keys:
            self._TICKERS[key] += self._generate_movement()
            if len(self._TICKERS_BUFFER[key]) >= self._TICKERS_BUFFER_SIZE:
                self._TICKERS_BUFFER[key].pop(0)
            self._TICKERS_BUFFER[key].append(self._TICKERS[key])

    def get_value_for(self, ticker_key: str) -> Optional[int]:
        return self._TICKERS.get(ticker_key)

    def get_buffer_for(self, ticker_key: str) -> Optional[List[int]]:
        return self._TICKERS_BUFFER.get(ticker_key)
