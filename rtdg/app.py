import asyncio
from typing import Any

from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from rtdg.config import Config
from rtdg.router import router
from rtdg.store import DequeTickersStore, TickersStore, NumpyTickersStore
from rtdg.ws import ConnectionManager


TickersStoreClass = {
    'default': TickersStore,
    'numpy': NumpyTickersStore,
    'deque': DequeTickersStore,
}


class App(FastAPI):
    config: Config
    templates: Jinja2Templates
    tickers_store: TickersStore
    ws_manager: ConnectionManager


def https_url_for(request: Request, name: str, **path_params: Any) -> str:
    http_url = request.url_for(name, **path_params)
    if request.app.config.https:
        return http_url.replace("http", "https", 1)
    return http_url


def startup(app: App, config: Config):
    async def f():
        print(f'Started with {config.tickers_store.value} store')
        app.config = config
        app.templates = Jinja2Templates(directory=config.templates_path)
        app.templates.env.globals['https_url_for'] = https_url_for
        app.tickers_store = TickersStoreClass[app.config.tickers_store](
            amount=config.tickers_amount,
            buffer_size=config.tickers_buffer_size
        )
        app.ws_manager = ConnectionManager()
    return f


def background(app: App):
    async def f():
        from rtdg.worker import ticker_worker
        asyncio.create_task(ticker_worker(app))
    return f


def shutdown(app: App, config: Config):
    async def f():
        pass
    return f


def make_app(config: Config) -> FastAPI:
    path_prefix = config.path_prefix
    app = App(title='Skanestas', version='0.1.0')
    app.add_event_handler('startup', startup(app=app, config=config))
    app.add_event_handler('startup', background(app=app))
    app.add_event_handler('shutdown', shutdown(app=app, config=config))
    app.mount('/static', StaticFiles(directory=config.static_path), name='static')
    app.include_router(router, prefix=config.path_prefix)
    return app

