import pytest

from rtdg.store import TickersStore


class TestTickersStore:
    def test_generate_movement(self):
        ts = TickersStore(amount=1)
        assert ts._generate_movement() in [-1, 1]

    @pytest.mark.parametrize(
        ('ticker_key', 'value', 'buffer'),
        [
            ('ticker_0', 0, [0]),
            ('ticker_1', 0, [0]),
            ('ticker_2', None, None),
        ]
    )
    def test_init(self, ticker_key, value, buffer):
        ts = TickersStore(amount=2)
        assert ts.get_value_for(ticker_key) == value
        assert ts.get_buffer_for(ticker_key) == buffer

    @pytest.mark.parametrize(
        ('amount', 'pad_zeroes'),
        [
            (1, 1),
            (10, 1),
            (11, 2),
            (99, 2),
            (100, 2),
            (101, 3),
        ]
    )
    def test_pad_zeroes(self, amount, pad_zeroes):
        ts = TickersStore(amount=amount)
        assert ts._pad_zeroes() == pad_zeroes

    @pytest.mark.parametrize(
        ('amount', 'index', 'key'),
        [
            (1, 0, 'ticker_0'),
            (11, 0, 'ticker_00'),
            (11, 6, 'ticker_06'),
            (111, 0, 'ticker_000'),
        ]
    )
    def test_key_from_index(self, amount, index, key):
        ts = TickersStore(amount=amount)
        assert ts._key_from_index(index) == key

    def test_tick(self, monkeypatch):
        ts = TickersStore(amount=1, buffer_size=2)

        monkeypatch.setattr(ts, '_generate_movement', lambda: 1)
        assert ts.get_value_for('ticker_0') == 0
        assert ts.get_buffer_for('ticker_0') == [0]

        ts.tick()
        assert ts.get_value_for('ticker_0') == 1
        assert ts.get_buffer_for('ticker_0') == [0, 1]

        ts.tick()
        assert ts.get_value_for('ticker_0') == 2
        assert ts.get_buffer_for('ticker_0') == [1, 2]

        ts.tick()
        assert ts.get_value_for('ticker_0') == 3
        assert ts.get_buffer_for('ticker_0') == [2, 3]
