import pytest

from rtdg.store import DequeTickersStore


class TestDequeTickersStore:
    @pytest.mark.parametrize(
        ('ticker_key', 'value', 'buffer'),
        [
            ('ticker_0', 0, [0]),
            ('ticker_1', 0, [0]),
            ('ticker_2', None, None),
        ]
    )
    def test_init(self, ticker_key, value, buffer):
        ts = DequeTickersStore(amount=2)
        assert ts.get_value_for(ticker_key) == value
        assert ts.get_buffer_for(ticker_key) == buffer

    def test_tick(self, monkeypatch):
        ts = DequeTickersStore(amount=1, buffer_size=2)

        monkeypatch.setattr(ts, '_generate_movement', lambda: 1)
        assert ts.get_value_for('ticker_0') == 0
        assert ts.get_buffer_for('ticker_0') == [0]

        ts.tick()
        assert ts.get_value_for('ticker_0') == 1
        assert ts.get_buffer_for('ticker_0') == [0, 1]

        ts.tick()
        assert ts.get_value_for('ticker_0') == 2
        assert ts.get_buffer_for('ticker_0') == [1, 2]

        ts.tick()
        assert ts.get_value_for('ticker_0') == 3
        assert ts.get_buffer_for('ticker_0') == [2, 3]
