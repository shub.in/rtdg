import pytest
from typing import Optional

import numpy as np
from numpy.typing import NDArray

from rtdg.store import NumpyTickersStore


DEFAULT_NUMBER_TYPE = np.int16


def _empty_buffer():
    return np.zeros(10, dtype=DEFAULT_NUMBER_TYPE)


class TestNumpyTickersStore:
    @staticmethod
    def _compare_ndarray(b: Optional[NDArray], buffer: Optional[NDArray]) -> bool:
        return (b == buffer).all() if b is not None else b == buffer

    @staticmethod
    def _list_to_ndarray(l: list, dtype=DEFAULT_NUMBER_TYPE) -> NDArray:
        return np.array(l, dtype=dtype)

    @pytest.mark.parametrize(
        ('ticker_key', 'value', 'buffer'),
        [
            ('ticker_0', 0, _empty_buffer()),
            ('ticker_1', 0, _empty_buffer()),
            ('ticker_2', None, None),
        ]
    )
    def test_init(self, ticker_key, value, buffer):
        ts = NumpyTickersStore(amount=2)
        assert ts.get_value_for(ticker_key) == value
        b = ts.get_buffer_for(ticker_key)
        assert self._compare_ndarray(b, buffer)

    def test_tick(self, monkeypatch):
        ts = NumpyTickersStore(amount=1, buffer_size=2)

        monkeypatch.setattr(ts, '_generate_movement', lambda: 1)
        assert ts.get_value_for('ticker_0') == 0
        assert self._compare_ndarray(ts.get_buffer_for('ticker_0'), self._list_to_ndarray([0, 0]))

        ts.tick()
        assert ts.get_value_for('ticker_0') == 1
        assert self._compare_ndarray(ts.get_buffer_for('ticker_0'), self._list_to_ndarray([0, 1]))

        ts.tick()
        assert ts.get_value_for('ticker_0') == 2
        assert self._compare_ndarray(ts.get_buffer_for('ticker_0'), self._list_to_ndarray([1, 2]))

        ts.tick()
        assert ts.get_value_for('ticker_0') == 3
        assert self._compare_ndarray(ts.get_buffer_for('ticker_0'), self._list_to_ndarray([2, 3]))
