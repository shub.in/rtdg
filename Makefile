.PHONY: test
test:
	pytest -vvv tests/

.PHONY: web
web:
	python3 web.py

.PHONY: install
install:
	pip install poetry
	poetry install
