document.addEventListener('DOMContentLoaded', function () {
    (function () {
        let chart = null;
        let chart_data = [];
        let connection = null;

        const tickers_list = document.getElementById('tickers_list');
        tickers_list.onchange = function (e) {
            ws_disconnect();
            ws_connect();
        }

        const price = document.getElementById('price');

        function target() {
            return tickers_list.value;
        }

        function on_message(event) {
            const data = JSON.parse(event.data);
            if (typeof data === 'object') {
                chart_data = data;
                price.innerHTML = data.slice(-1);
                chart = c3.generate({
                    bindto: '#chart',
                    data: {
                        columns: [
                            [target()].concat(chart_data)
                        ]
                    },
                    padding: {
                        right: 20
                    }
                });
            } else if (typeof data === 'number') {
                price.innerHTML = data;
                if (chart_data.length < BUFFER_SUZE) {
                    chart_data.push(data);
                    chart.load({
                        columns: [
                            [target()].concat(chart_data)
                        ]
                    })
                } else {
                    chart_data = chart_data.slice(1);
                    chart_data.push(data);
                    chart.flow({
                        columns: [
                            [target()].concat(data)
                        ]
                    })
                }
            }
        }

        function ws_connect() {
            let URL = window.location.href;
            URL = URL.startsWith('https') ? URL.replace('https', 'wss') : URL.replace('http', 'ws');
            URL = URL.endsWith('/') ? URL.slice(0, -1) : URL;
            connection = new WebSocket(`${URL}/ws/${target()}`);
            connection.onmessage = on_message
        }

        function ws_disconnect() {
            if (connection) {
                connection.close();
                connection = null;
            }
        }

        ws_connect();
    })()
});
