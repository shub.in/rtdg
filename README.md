# RTDG  (Real-Time Data Generator)

Прототип сервиса по получению и предпросмотру торговых данных.

**Задание состоит из следующих этапов:**
1. Реализация сервиса по генерации realtime данных. 
Сервис должен возвращать раз в секунду цены для 100 искусственных торговых инструментов: ticker_00, ticker_01, …, ticker_99.
В качестве функции изменения цены для каждого инструмента предлагается использовать следующий код:
    ```python
    from random import random
    
    def generate_movement():
        movement = -1 if random() < 0.5 else 1
        return movement
    ```
    Код выше моделирует изменение цены за один шаг времени. Цена каждого инструмента будет складываться кумулятивно из всех изменений. Цену в начальный момент времени принимаем равной нулю.

2. Реализация веб-сервиса по визуализации цены в режиме реального времени.

    Необходимо вывести:
   * Селектор инструмента в виде выпадающего списка
   * График цены по выбранному инструменту от начального момента с добавлением данных по мере поступления. 

### Setup & Install

**Local:**
```shell
git clone git@gitlab.com:shub.in/rtdg.git
cd rtdg
make install
make test
make web
```

**Docker:**
```shell
git clone git@gitlab.com:shub.in/rtdg.git
cd rtdg
docker build -t rtdg .
docker run --rm -e "RTDG_HOST=0.0.0.0" -p 8080:8080 -it rtdg
```

**Available environment variables:**
```shell
RTDG_HOST=0.0.0.0
RTDG_PORT=8080
RTDG_TICKERS_AMOUNT=100
RTDG_TICKERS_BUFFER_SIZE=100
```
