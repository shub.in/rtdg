import uvicorn
from rtdg.app import make_app
from rtdg.config import read_config


if __name__ == '__main__':
    config = read_config()
    uvicorn.run(app=make_app(config), host=config.host, port=config.port)

